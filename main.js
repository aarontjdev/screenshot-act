const Apify = require('apify');
const AWS = require('aws-sdk');

// Set AWS credentials by using apify secrets.
const access_key_id = process.env.AWS_ACCESS_KEY_ID;
const secret_access_key = process.env.AWS_SECRET_ACCESS_KEY;

// Set AWS credentials on AWS instance
AWS.config.update({
    accessKeyId: access_key_id,
    secretAccessKey: secret_access_key
});


Apify.main(async () => {
    const { pages } = await Apify.getInput();
    const requests = await prepRequestList(pages);

    const requestList = new Apify.RequestList({
        sources: requests
    });

    await requestList.initialize();

    const crawler = new Apify.BasicCrawler({
        requestList,
        handleRequestTimeoutSecs:120,
        maxRequestRetries:1,
        maxConcurrency:5,

        handleRequestFunction: async({request}) => {
            const pageInfo = request.userData.page.input;
            
            // BEFORE PAGE IS NAVIGATED TO
            // Create browser instance with or without userAgent or proxy set.
            const launchOptions = {
                proxyUrl: pageInfo.proxy,
                userAgent: pageInfo.userAgent
            }

            const browser = await Apify.launchPuppeteer(launchOptions)
            const page = await browser.newPage();

            await page.setCookie(...pageInfo.cookies);

            var response = await page.goto(request.url, { timeout: pageInfo.timeout });
            request.headers = response.headers();
            request.code = response.status();

            // AFTER PAGE IS NAVIGATED TO

            if(pageInfo.viewport.height || pageInfo.viewport.width){
                await page.setViewport({
                    width: pageInfo.viewport.width,
                    height: pageInfo.viewport.height
                });
            }

            // Set delay
            if(pageInfo.delay > 0){
                await new Promise(resolve => setTimeout(resolve, pageInfo.delay));
            }

            // Lazy load
            if(pageInfo.useLazyLoading){
                await lazyLoad(page);
            }

            // Click elements
            if(pageInfo.elementsToClick){
                await clickElements(page, request, pageInfo.elementsToClick);
            }

            // Hide elements
            if(pageInfo.elementsToHide){
                await hideElements(page, request, pageInfo.elementsToHide);
            }

            // Take screenshot
            var ssResult = await screenshotToS3(page, request, pageInfo.storage, pageInfo.viewport);

            console.log(`SS RESULT HEREE: ${ssResult}`);
            await browser.close()

            // Generate output
            var output = {
                id: request.userData.id,
                input: request.userData.page,
                output:{
                    response: {
                        headers: response.headers(),
                        code: response.status()
                    }
                },
                storage: {
                    url: ssResult
                }
            }
            
            await Apify.pushData(output);
        },

        // This function is called if the page processing failed more than maxRequestRetries+1 times.
        handleFailedRequestFunction: async ({ request }) => {
            console.log('handling failed request');

            // Generate outputs
            var output = {
                id: request.userData.id,
                input: request.userData.page,
                output:{
                    response: {
                        headers: request.headers,
                        code: request.code
                    },
                    errors: request.errorMessages
                }
            }

            await Apify.pushData(output);
        },
    });


    // Run the crawler and wait for it to finish.
    await crawler.run();

    console.log('Crawler finished.');
});

/**
 * Validates field for a page object
 * @param {Object} page
 */
async function validatePageConfig(page){
    const defaultVal = {
        timeout: 60000,
        viewWidth: 1280,
        viewHeight: 50000,
        delay: 0
    }

    // Assign user input or default values
    // Integers
    page.input.timeout = page.input.timeout || defaultVal.timeout;
    page.input.viewport.height = page.input.viewport.height || defaultVal.viewHeight;
    page.input.viewport.width = page.input.viewport.width || defaultVal.viewWidth;
    page.input.delay = page.input.delay || defaultVal.delay;

    // Booleans
    page.input.useLazyLoading = page.input.useLazyLoading || true;

    // Objects
    // Verify if cookies exist
    page.input.cookies = Object.entries(page.input.cookies).length > 0 && page.input.cookies.constructor === Object ? page.input.cookies : null;
    // Reformat cookies
    page.input.cookies = Object.entries(page.input.cookies).map( cookie => {
        return {
            'url': page.input.url,
            'name': cookie[0],
            'value' : cookie[1]
        }
    });;
     
    // Strings
    page.input.userAgent = page.input.userAgent || null
    page.input.proxy = page.input.proxy ? 'http://' + page.input.proxy : null;

    // Arrays
    page.input.elementsToClick = page.input.elementsToClick || null;
    page.input.elementsToHide = page.input.elementsToHide || null;
    

    // Validate input for timeout
    if(page.input.timeout  < 1){
        console.log('Timeout may not be less than 1');
        page.input.timeout = 1;
    } else if (page.input.timeout > 60000){
        console.log('Timeout may not be greater than 60,000');
        page.input.timeout = 60000;
    }

    // Validate input for delay
    if(page.input.delay  < 0){
        console.log('Delay value changed to 0. Delay can not have a negative value.');
        page.input.delay = 0;
    } else if (page.input.delay > 60000){
        console.log('Delay may not be greater than 60,000');
        page.input.delay = 60000;
    }

    // Validate input for viewport height
    if(page.input.viewport.height  < 1){
        console.log('Viewport height may not be less than 1');
        page.input.viewport.height = 1;
    } else if (page.input.viewport.height > 50000){
        console.log('Viewport height may not be greater than 50,000');
        page.input.viewport.height = 50000;
    }

    // Validate input for viewport width
    if(page.input.viewport.width  < 1){
        console.log('Viewport width may not be less than 1');
        page.input.viewport.width = 1;
    } else if (page.input.viewport.width > 1920){
        console.log('Viewport width may not be greater than 1920');
        page.input.viewport.width = 1920;
    }

    return page
}

/**
 * Builds list of requests from user input pages.
 * @param {*} pages 
 */
async function prepRequestList(pages){
    var requests = [];

    if(pages.length > 0){
        // Loop through pages
        for(let page of pages){
            page = await validatePageConfig(page);

            // Instantiate request
            let request = new Apify.Request({
                url: page.input.url,
                userData:{
                    page: page
                }
            });

            // Add request to list
            requests.push(request);
        } 
    } else {
        throw "ERROR: No pages to crawl in input.";
    }
    
    return requests
}

/**
 * Function takes a page object
 * An array of element selectors
 * And clicks all elements on the page
 * @param {Object} page 
 * @param {Array} selectors 
 */
async function clickElements(page, request){
    var selectors = request.userData.page.input.elementsToClick;

    for(let selector of selectors){
        try {
            await page.$eval(selector, element => element.click());
        } catch(err){
            await request.pushErrorMessage(err.message);
            console.log(`ERROR: with clicking selector "${selector}"`);
        }
    }
}

/**
 * Function takes a page object
 * An array of element selectors
 * And hides all elements on the page
 * @param {Object} page 
 * @param {Array} selectors 
 */
async function hideElements(page, request){
    var selectors = request.userData.page.input.elementsToHide;

    for(let selector of selectors){
        try {
            await page.$eval(selector, element => element.style.display = "none !important");
        } catch(err){
            await request.pushErrorMessage(err.message);
            console.log(`ERROR: with hiding selector "${selector}"`);
        }
    }
}

/**
 * Take screenshot of page, then upload it to an AWS S3 bucket
 * returns an URL if the upload was successful, otherwise it returns an error message.
 * @param {Object} page 
 * @param {Object} storage
 * @param {Object} viewport
 *  */
async function screenshotToS3(page, request, storage, viewport){
    const screenshot = await page.screenshot({
        type:'png',
        clip:{
            x:0,
            y:0,
            height: viewport.height,
            width: viewport.width
        }
    });

    var s3 = new AWS.S3();
    
    return s3.upload({
        Bucket:storage.bucket,
        Body: screenshot,
        Key: storage.object + '.png'
    }).promise().then((data) => {
        console.log(`Screenshot of ${page.url()} saved to ${data.Location}`);
        return data.Location;
    }).catch(async (err) => {
        await request.pushErrorMessage(err.message);
    });
}

/**
 * Scrolls a page that is lazy loaded.
 * Details can be found at the url below.
 * https://www.screenshotbin.com/blog/handling-lazy-loaded-webpages-puppeteer
 * @param {*} page 
 */
async function lazyLoad(page){
    await new Promise(resolve => setTimeout(resolve, 2000));
    // Get the height of the rendered page
    const bodyHandle = await page.$('body');
    const { height } = await bodyHandle.boundingBox();
    await bodyHandle.dispose();

    await new Promise(resolve => setTimeout(resolve, 2000));
    // Scroll one viewport at a time, pausing to let content load
    const viewportHeight = page.viewport().height;
    let viewportIncr = 0;

    while (viewportIncr + viewportHeight < height) {
        await page.evaluate(_viewportHeight => {
            window.scrollBy(0, _viewportHeight);
        }, viewportHeight);
        await new Promise(resolve => setTimeout(resolve, 100));
        viewportIncr = viewportIncr + viewportHeight;
    }

    // Some extra delay to let images load
    await new Promise(resolve => setTimeout(resolve, 200));
}
