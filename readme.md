# Specification of screenshot actor (v0.0.1)
==============================

This specification describes the input and output fields of the screenshot actor. The actor creates screenshots of one or more webpages in PNG format and stores the screenshots to AWS S3. The actor uses headless Chrome + Puppeteer. The actor creates screenshots of webpages concurrently, not sequentially. The number of webpages can be quite large, so it should use PuppeteerCrawler from the Apify SDK to automatically scale as needed.

# Input

The actor creates one or more screenshots using the input fields underneath. Please note that each webpage can have different settings.

## Fields
Name | Type | Values allowed | Default value | Required | Description
--|--|--|--|--|--
pages | List of objects | n/a | n/a | Yes; at least one object | Container for the page fields
pages[*].id | String | n/a | n/a | Yes | Identifier of the webpage. This identifier serves the caller of the actor and is not used by the actor
pages[*].input | Object | n/a | n/a | Yes | Container for the page's input fields
pages[*].input.url | String | n/a | n/a | Yes | URL of the webpage to take a screenshot of
pages[*].input.timeout | Integer | Integer between `1` and `60000` | `60000` | No | Number of milliseconds to wait before the page responds. If the timeout is reached, the request should be cancelled
pages[*].input.cookies | Object of strings, each having a name and a value | n/a | n/a | No | Values to use for setting the `Set-Cookie` header in the request, consisting of one or more cookies
pages[*].input.userAgent | String | n/a | n/a | No | Value to use for setting the `User-Agent` header in the request
pages[*].input.proxy | String | n/a | n/a | No | Address of a proxy server to make the request from, e.g. in format `[user]:[password]@[address]:[port]`
pages[*].input.useLazyLoading | Boolean | `true` or `false` | `true` | No | If `true`, the actor must first scroll through the page so as to trigger any lazy loaded elements before taking a screenshot
pages[*].input.viewport | Object | n/a | n/a | Yes | Container for the viewport fields
pages[*].input.viewport.width | Integer | Integer between `1` and `1920` | `1280` | No | Viewport width of the browser in pixels
pages[*].input.viewport.height | Integer | Integer between `1` and `50000` | `50000` | No | Viewport height of the browser in pixels. If set and the actual height is smaller than the height specified, the actual height is used instead. If not set, the height is detected/calculated automatically ('full page height'), with a maximum of `50000` (to prevent an extremely long screenshot)
pages[*].input.delay | Integer | Integer between `0` and `60000` | 0 | No | Number of milliseconds to wait after the webpage is loaded before taking a screenshot
pages[*].input.elementsToClick | List of strings | n/a | n/a | No | Each value in the list is a CSS selector pattern. If set, the actor must first look-up the elements matching the selectors and trigger them before taking a screenshot. The click action can e.g. be used to display a specific element, e.g. a slide of a carousel that is not visible when a webpage is loaded
pages[*].input.elementsToHide | List of strings | n/a | n/a | No | Each value in the list is a CSS selector pattern. If set, the actor must first look-up the elements matching the selectors and set their style to `display:none !important;` before taking a screenshot. The hide action can e.g. be used to hide cookie consent popups
pages[*].input.storage | Object | n/a | n/a | Yes | Container for the storage fields
pages[*].input.storage.service | String | `s3` | n/a | Yes | Storage service. Currently there's only one, AWS S3. The secret environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are used for authorization
pages[*].input.storage.bucket | String | n/a | n/a | Yes | Name of the S3 bucket to store the screenshot in
pages[*].input.storage.object | String | n/a | n/a | Yes | Name of the S3 object to store the screenshot

## Example
```json
{
    "pages": [
        {
            "id": "dfff1c8d-0322-4301-98f1-e12c852852ef",
            "input": {
                "url": "https://www.bbc.com/",
                "timeout": 60000,
                "cookies": {
                    "userid": "dfb613ac-d896-4062-8f32-1417af58039a",
                    "ckns_privacy": "1"
                },
                "userAgent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
                "proxy": "john:doe@localhost:1234",
                "useLazyLoading": true,
                "viewport": {
                    "width": 1280,
                    "height": 1024
                },
                "delay": 0,
                "elementsToClick": [".intro", "li:nth-child(2) > div > a"],
                "elementsToHide": ["#cookie_consent", ".banner"],
                "storage": {
                    "service": "s3",
                    "bucket": "someBucket",
                    "object": "some/path/to/some/screenshot.png"
                }
            }
        }
    ]
}
```

## Handling lazy loading

Lazy loading is essential to the actor's working. The input field `input.useLazyLoading` instructs the browser to first scroll to a certain offset and thus ensures any lazy elements are triggered. This is different from the `waitUntil` method of Puppeteer; this method waits until e.g. a load event is triggered, but this doesn't necessarily mean that all elements have been triggered. For inspiration how to do this, see https://www.screenshotbin.com/blog/handling-lazy-loaded-webpages-puppeteer (but other solutions may also work).

# Output

The actor generates a dataset where each record contains the following information:

## Fields
Name | Type | Values allowed | Default value | Required | Description
--|--|--|--|--|--
pages | List of objects | n/a | n/a | Yes; at least one object | Container for the page fields
pages[*].id | String | n/a | n/a | Yes | Identifier of the webpage
pages[*].input | Object | n/a | n/a | Yes | Container for the page's input fields, containing a copy of the original fields. Omitted for brevity; see 'Input' for the exact fields
pages[*].output | Object | n/a | n/a | Yes | Container for the page's output fields
pages[*].output.response | Object | n/a | n/a | Yes | Container of the page's response fields
pages[*].output.response.headers | Object of strings, each having a name and a value | n/a | n/a | No | The page's response headers, e.g. `contentType` or `contentLanguage`
pages[*].output.response.code | Integer | n/a | n/a | No | The page's response code, e.g. `200` or `500`
pages[*].output.storage.url | String | n/a | n/a | No | S3 URL of the screenshot. Not set if the screenshot could not be taken
pages[*].output.errors | List of strings | n/a | n/a | No | Each value in the list is the message of an error that occurred while taking the screenshot (e.g. a Puppeteer error or a HTTP error). Not set if the screenshot was taken

## Example
```json
{
    "pages": [
        {
            "id": "dfff1c8d-0322-4301-98f1-e12c852852ef",
            "input": {
                // Copy of the input fields
            },
            "output": {
                "response": {
                    "headers": {
                        "contentType": "text/html"
                    },
                    "code": 200
                },
                "storage": {
                    "url": "https://someBucket.s3.amazonaws.com/some/path/to/some/screenshot.png"
                },
                "errors": ["Some error"]
            }
        }
    ]
}
```

# Example URLs

The URLs underneath can be used for testing screenshots. Please note that these URLs may not be accessible from every location; use a proxy if you have to.

## Fields
URL | Interesting because...
--|--
https://www.bbc.com/ | Lazy loading
https://twitter.com/espn | Lazy loading
https://www.vudu.com/ | Lazy loading (only accessible from the US; please use a proxy if you're not from there)
https://www.cnn.com/ | Lazy loading (only accessible from the US; please use a proxy if you're not from there. The Photos section uses lazy loading)
https://www.chicagotribune.com/ | Lazy loading (only accessible from the US; please use a proxy if you're not from there)
https://www.facebook.com/ESPN/ | Lazy loading
http://www.espn.com/ | Hide cookie consent popup in the footer
https://www.michaelkors.com/ | Location-specific redirect, and hide cookie consent popup in the footer
https://www.att.com/shop/wireless/accessories.html | Redirects to https://www.att.com/accessories/
https://www.currys.co.uk/gbuk/index.html | Select slide 3 in the carousel (e.g. using selector `li.dc-slider__tab:nth-child(3) > div:nth-child(1) > a:nth-child(1)`)
https://www.bestbuy.com/?intl=nosplash | Select slide 2 in the carousel (e.g. using selector `#widget-72b016a6-e803-4a7a-b23c-fc9d60107a95 > div.rotating-panel.paused.auto-rotate-disabled > div:nth-child(5) > div > button`)